import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = 'SE331';
  courseName = 'COMPO-BASED SOFTWARE DEV';
  students = [{
    id: 1,
    studentId: '602110507',
    name: 'Prayuht',
    surname: 'Tu',
    gpa: 4.00
  }, {
    id: 2,
    studentId: '602110509',
    name: 'Pu',
    surname: 'Priya',
    gpa: 2.12
  }, {
    id: 3,
    studentId: '602115012',
    name: 'Thitiwut',
    surname: 'Chutipongwanit',
    gpa: 1.12
  }
  ];

  averageGpa() {
    let sum= 0;
    for(let student of this.students){
      sum += student.gpa;
    }
    return sum/this.students.length;
  }

  getCurrentTime(){
    return new Date();
  }
}
